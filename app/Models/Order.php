<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class Order extends Model
{
    use HasFactory;

    protected $table = 'orders';
    protected $primaryKey = "id";
    protected $keyTypes = "string";
    protected $fillable = [
        'nama_penerima',
        'handphone',
        'provinsi',
        'kota',
        'kecamatan',
        'kodepos',
        'alamat',
        'courier_id',
        'ongkir',
        'harga_total',
        'user_id',
    ];
    public $incrementing = false;
    public $timestamps = true;

    protected static function boot()
    {
        parent::boot();


        static::creating(function($model){
            if(empty($model->{$model->getKeyName()})){
                $model->{$model->getKeyName()} = Str::uuid();
            }
            $model->user_id = auth()->user()->id;
        });
    }

    public function products()
    {
        return $this->belongsToMany(Product::class, 'order_has_products', 'order_id', 'product_id')->withPivot('qty');
    }
    public function packages()
    {
        return $this->belongsToMany(Package::class, 'order_has_products', 'order_id', 'package_id')->withPivot('qty');
    }

    
}
